/**
 * Our base Gruntfile. Use this to init your project
 *
 * @author David Faber
 * @company Peritus Webdesign
 */

module.exports = function (grunt) {
    'use strict';

    // Project configuration
    if (grunt.file.exists('project.json')) {
        grunt.initConfig({
            project: grunt.file.readJSON('project.json'),

            auth: {
                bitbucket: grunt.file.readJSON('Settings/.auth/bitbucket.json')
            },

            dirs: {
                files: 'Tools/Files/',
                plugins: '<%= dirs.wordpress %>wp-content/plugins/',
                projectJSON: 'project.json',
                templates: 'Tools/Templates/',
                themes: '<%= dirs.wordpress %>wp-content/themes/',
                wordpress: 'Public/',
                wpZip: './wordpress.zip'
            },

            templateTags: {
                domain: '{{domain}}',
                envConfig: '{{envConfig}}',
                environment: '{{environment}}',
                locale: '{{locale}}',
                projectFolder: '{{projectFolder}}',
                salt: '{{salt}}',
                slug: '{{slug}}',
                tablePrefix: '{{tablePrefix}}'
            },

            templates: {
                apacheConfig: grunt.file.read('Tools/Templates/apacheConfig.template'),
                gitIgnore: '<%= dirs.templates %>gitignore.template',
                htaccessBase: '<%= dirs.templates %>htaccessBase.template',
                humans: '<%= dirs.templates %>humans.template',
                robots: '<%= dirs.templates %>robots.template',
                wpConfig: grunt.file.read('Tools/Templates/wp-config.template')
            },

            files: {
                visualComposer: '<%= dirs.files %>js_composer.zip'
            },

            commands: {
                apache: {
                    testConfig: 'apache2ctl configtest',
                    reloadConfig: 'service apache2 reload',
                    restart: 'service apache2 restart'
                }
            },

            pwRepo: grunt.file.readJSON('Settings/repoSettings.json'),

            wpRepo: {
                url: 'https://downloads.wordpress.org/release/',
                plugins: 'https://downloads.wordpress.org/plugin/'
            },

            wpApi: {
                url: 'https://api.wordpress.org/',

                plugin: 'plugins/info/1.0/',
                version: 'core/version-check/1.7/',
                salt: 'secret-key/1.1/salt/'
            }
        });
    } else {
        grunt.initConfig({});
    }


    // Load internal tasks
    grunt.loadTasks('Tools/Grunt/Build');
    grunt.loadTasks('Tools/Grunt/Environment');
    grunt.loadTasks('Tools/Grunt/Git');
    grunt.loadTasks('Tools/Grunt/Init');
    grunt.loadTasks('Tools/Grunt/Plugins');
    //grunt.loadTasks('Tools/Grunt/Themes');
    grunt.loadTasks('Tools/Grunt/SQL');

    /**
     * Our Setup Task
     */
    grunt.registerTask('setup', function (env, withDB, action) {
        'use strict';

        // Check if project.json exists
        if (!grunt.file.exists('project.json')) {
            grunt.fail.fatal('There is no project.json. Use \'grunt projectJSON\' to create one!');
        }

        grunt.task.run(
            'getWP',
            'cleanupWP',
            'wpConfig',
            'generateHtaccess',
            'generateHumans',
            'generateRobots',
            'installPlugins',
            //'installTheme',
            'initGit'
        );

        if (env) {
            var taskName = 'envConfig:' + env;

            if (withDB) {
                taskName += ':' + withDB;
            }

            if (action) {
                taskName += ':' + action;
            }

            grunt.task.run(taskName);
        }
    });
};