------------------------------------------------------------------------------------------------------------------------
# Table of Contents

* [Setup Automatism](#markdown-header-setup-automatism)
    * [Defining the project](#markdown-header-defining-the-project)
    * [Setting up the project](#projectSetup)
* [Building Automatism](#markdown-header-building-automatism)

------------------------------------------------------------------------------------------------------------------------
# Setup Automatism
There are several grunt-tasks to run an automatic setup. The main ones are described in this document.

## Defining the project
To create a project-definition-file (project.json) run the following command within the project-folder.
```
$ grunt projectJSON
```

The prompt will guide you through the process.

## Setting up the project
For setting up the project use the following command:
```
$ grunt setup
```

`grunt setup` supports 3 optional parameters:

* Environment
* Database
* Action

### Environment
The environment takes 3 different values:

* dev
* integration
* live

When an environment-name is handed to the setup-task, apache-config-files will be created and uploaded to the given environment.

**Important:** In order to upload the files a valid ssh-config must be present for the given environment.

**Example usage:**
```
$ grunt setup:dev
```

### Database
Create the database remotely on the given environment.

**Example usage:**
```
$ grunt setup:dev:db
```

### Action
The action parameter defines what should be done after uploading the apache-config OR what should be done with the apache-config. There are 3 possible values:

* start: Start the given environment in your default browser
* echo: **DO NOT UPLOAD** the apache-config, but echo it on command line
* write: **DO NOT UPLOAD** the apache-config, but write it to a file

**Example usage:**
```
$ grunt setup:dev:db:start
```

------------------------------------------------------------------------------------------------------------------------
# Building Automatism
Building automatisms enable you to build sass- and javascript-files for testing (with sourcemaps) or for live (minified).

### Building Sass

