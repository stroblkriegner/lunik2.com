/**
 * Tasks to control the db
 *
 * @author David Faber
 * @company Peritus Webdesign
 */

module.exports = function (grunt) {
    'use strict';

    // Load external tasks
    grunt.loadNpmTasks('grunt-ssh');

    /**
     * Task to create the db
     *
     * @param string env The environment to set up
     */
    grunt.registerTask('createDB', function (env) {
        'use strict';

        // Dependencies
        this.requiresConfig(
            'project.environments.' + env + '.ssh.host',
            'project.environments.' + env + '.ssh.user',
            'project.environments.' + env + '.ssh.key',
            'project.environments.' + env + '.mysql.db',
            'project.environments.' + env + '.mysql.user',
            'project.environments.' + env + '.mysql.password',
            'project.environments.' + env + '.mysql.host'
        );

        // Get the config
        var ssh = grunt.config('project.environments.' + env + '.ssh');
        var mysql = grunt.config('project.environments.' + env + '.mysql');

        // Build command
        grunt.log.subhead('Building SQL command for creating db "' + mysql.db + '" on environment ' + env);
        var command = 'echo "CREATE DATABASE ' + mysql.db + ';" | mysql -h ' + mysql.host + ' -u ' + mysql.user
            + ' --password=' + mysql.password;
        grunt.log.ok('Command built successfully!');

        // Configure tasks
        grunt.log.subhead('Configuring sshexec task.');
        grunt.config(
            'sshexec',
            {
                createDB: {
                    command: command,
                    options: {
                        host: ssh.host,
                        username: ssh.user,
                        privateKey: grunt.file.read(ssh.key)
                    }
                }
            }
        );
        grunt.log.ok('Everything configured!');

        // Trigger the sshexec task
        grunt.task.run('sshexec:createDB');
    });
};