/**
 * Install a base theme
 *
 * @author David Faber
 * @company Peritus Webdesign
 */
var request = require('request-json');

module.exports = function (grunt) {
    'use strict';

    // Load external tasks
    grunt.loadNpmTasks('grunt-zip');
    grunt.loadNpmTasks('grunt-contrib-rename');

    // Define themes
    var themes = grunt.file.readJSON('Settings/themes.json');

    /**
     * Our combined task
     */
    grunt.registerTask(
        'installTheme',
        [
            'setupThemePrompt',
            'prompt:theme',
            'downloadThemeFiles',
            'cleanupTheme',
            'renameThemeFolder'
        ]
    );

    /**
     * Setting up the theme prompt
     */
    grunt.registerTask('setupThemePrompt', function () {
        'use strict';

        grunt.config(
            'prompt.theme',
            {
                options: {
                    questions: [
                        {
                            config: 'theme',
                            type: 'checkbox',
                            message: 'Choose a theme to install:',
                            choices: themes
                        }
                    ]
                }
            }
        );
    });

    /**
     * Download selected theme
     */
    grunt.registerTask('downloadThemeFiles', function () {
        'use strict';

        // Dependencies
        this.requires('prompt:theme');
        this.requiresConfig(
            'auth.bitbucket.username',
            'auth.bitbucket.password',
            'dirs.themes',
            'pwRepo.url',
            'pwRepo.headZip',
            'theme'
        );

        // Get the config
        var bitbucketAuth = grunt.config('auth.bitbucket');
        var themesDir = grunt.config('dirs.themes');
        var pwRepo = grunt.config('pwRepo');
        var theme = grunt.config('theme');

        // This task is async
        var done = this.async();

        // Download the selected Theme
        grunt.log.subhead('Downloading Theme with slug: ' + theme);
        var pwRepoClient = request.newClient(pwRepo.url);
        pwRepoClient.setBasicAuth(bitbucketAuth.username, bitbucketAuth.password);
        pwRepoClient.saveFile(
            pwRepo.url + theme + pwRepo.headZip,
            themesDir + theme + '.zip',
            function (error) {
                if (error) {
                    grunt.fail.fatal('Could not download theme from: ' + pwRepo.url + theme + pwRepo.headZip);
                }

                grunt.log.ok('Theme downloaded correctly.');

                // Unzip theme
                grunt.log.subhead('Configuring unzip-task for theme');
                grunt.config(
                    'unzip.theme',
                    {
                        src: themesDir + theme + '.zip',
                        dest: themesDir
                    }
                );
                grunt.task.run('unzip:theme');
                grunt.log.ok('Configured!');

                done();
            }
        );
    });

    /**
     * Rename cryptic theme-folder to nice one
     */
    grunt.registerTask('renameThemeFolder', function () {
        'use strict';

        // Dependencies
        this.requiresConfig('dirs.themes', 'theme');

        // Get the config
        var themesDir = grunt.config('dirs.themes');
        var theme = grunt.config('theme');

        // Rename theme folder
        var folders = grunt.file.expand(themesDir + '*' + theme + '*');

        if (folders[0]) {
            grunt.log.subhead('Configuring rename task for: ' + folders[0] + ' > ' + themesDir + theme);
            grunt.config(
                'rename.theme',
                {
                    src: folders[0],
                    dest: themesDir + theme
                }
            );

            grunt.task.run('rename:theme');
            grunt.log.ok('Configured.');
        }
    });

    /**
     * Cleanup theme-zip
     */
    grunt.registerTask('cleanupTheme', function () {
        'use strict';

        // Dependencies
        this.requiresConfig('dirs.themes', 'theme');

        // Get the config
        var themesDir = grunt.config('dirs.themes');
        var theme = grunt.config('theme');

        // Delete the file
        grunt.file.delete(themesDir + theme + '.zip');
    });
};