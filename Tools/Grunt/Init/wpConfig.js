/**
 * Tasks to create a the wp-config from a template
 *
 * @author David Faber
 * @company Peritus Webdesign
 */
module.exports = function (grunt) {
    'use strict';

    /**
     * Our combined task
     */
    grunt.registerTask(
        'wpConfig',
        [
            'configureEnvironments',
            'configureTablePrefix',
            'configureSalt',
            'configureLocale',
            'writeConfig'
        ]
    );

    /**
     * A task to get the salt and replace it in the config-template
     */
    grunt.registerTask('configureEnvironments', function () {
        'use strict';

        // Dependencies
        this.requiresConfig(
            'project.environments',
            'templates.wpConfig',
            'templateTags.envConfig'
        );

        // Get config
        var wpConfig = grunt.config('templates.wpConfig');
        var environments = grunt.config('project.environments');
        var tag = grunt.config('templateTags.envConfig');

        // Build the sql-config
        grunt.log.subhead('Building environment-config');
        var envCount = Object.keys(environments).length;
        var envConfig = '';
        var i = 1;
        var single = false;

        if (envCount == 1) {
            single = true;
        }

        for (var envSlug in environments) {
            // Env Dependencies
            this.requiresConfig(
                'project.environments.' + envSlug + '.env',
                'project.environments.' + envSlug + '.mysql.db',
                'project.environments.' + envSlug + '.mysql.user',
                'project.environments.' + envSlug + '.mysql.password',
                'project.environments.' + envSlug + '.mysql.host'
            );

            // Env Config
            var env = grunt.config('project.environments.' + envSlug + '.env');
            var mysql = grunt.config('project.environments.' + envSlug + '.mysql');
            var domainCheck = grunt.config('project.environments.' + envSlug + '.domainCheck');
            var domain = grunt.config('project.environments.' + envSlug + '.domain');
            var debug = grunt.config('project.environments.' + envSlug + '.debug');

            // Build config
            var last = false;
            if (single) {
                last = true;
            } else if(i == 1) {
                envConfig += 'if (getenv(\'APPLICATION_ENV\') == \'' + env + '\'';
            } else if (i == envCount) {
                envConfig += ' else {\n';
                last = true;
            } else {
                envConfig += ' else if (getenv(\'APPLICATION_ENV\') == \'' + env + '\''
            }

            if (!last && domainCheck && domain) {
                envConfig += ' || strpos($_SERVER[\'SERVER_NAME\'], \'' + domain + '\') !== FALSE';
            }

            if (!last) {
                envConfig += ') {\n';
            }

            envConfig += '    // ' + env + '-db\n';
            envConfig += '    define(\'DB_NAME\', \'' + mysql.db + '\');\n';
            envConfig += '    define(\'DB_USER\', \'' + mysql.user + '\');\n';
            envConfig += '    define(\'DB_PASSWORD\', \'' + mysql.password + '\');\n';
            envConfig += '    define(\'DB_HOST\', \'' + mysql.host + '\');\n';

            if (debug) {
                envConfig += '\n    // Debug is on\n';
                envConfig += '    define(\'WP_DEBUG\', true);\n';
            } else {
                envConfig += '\n    // Debug is off (hide all php-errors)\n';
                envConfig += '    error_reporting(0);\n';
                envConfig += '    @ini_set(\'display_errors\', 0);\n';
            }

            if (!single) {
                envConfig += '}';
            }

            grunt.log.ok('Successfully added environment: ' + env);

            i++;
        }

        // Write config
        wpConfig = wpConfig.replace(tag, envConfig);
        grunt.config('templates.wpConfig', wpConfig);

        grunt.log.ok('All environments added successfully!');
    });

    /**
     * A task to set the table-prefix in the config-template
     */
    grunt.registerTask('configureTablePrefix', function () {
        'use strict';

        // Dependencies
        var md5 = require('MD5');
        this.requiresConfig(
            'project.slug',
            'templates.wpConfig',
            'templateTags.tablePrefix'
        );

        // Get the config
        var wpConfig = grunt.config('templates.wpConfig');
        var slug = grunt.config('project.slug');
        var tag = grunt.config('templateTags.tablePrefix');

        // Create the tablePrefix
        grunt.log.subhead('Creating table-prefix');
        var seed = md5(slug).substring(0, 5);
        var tablePrefix = '';
        var possible = 'CKPRcfjkqz';

        for(var i=0; i<5; i++) {
            var pos = seed.charAt(i);

            if (isNaN(pos)) {
                tablePrefix += pos;
            } else {
                tablePrefix += possible.charAt(pos);
            }
        }

        tablePrefix += '_';

        grunt.log.ok('Table-Prefix used is: ' + tablePrefix);

        // Write config
        wpConfig = wpConfig.replace(tag, tablePrefix);
        grunt.config('templates.wpConfig', wpConfig);

        grunt.log.ok('Table-Prefix added successfully!');
    });

    /**
     * A task to get the salt and replace it in the config-template
     */
    grunt.registerTask('configureSalt', function () {
        'use strict';

        // Dependencies
        var request = require('request');
        this.requiresConfig(
            'wpApi.url',
            'wpApi.salt',
            'templates.wpConfig',
            'templateTags.salt'
        );

        // Get the config
        var wpApi = grunt.config('wpApi.url');
        var endpoint = grunt.config('wpApi.salt');
        var wpConfig = grunt.config('templates.wpConfig');
        var tag = grunt.config('templateTags.salt');

        // This task is async
        var done = this.async();

        // Get the salt
        grunt.log.subhead('Getting salt from ' + wpApi + endpoint);
        request(wpApi + endpoint, function (error, response, body) {
            if (error || response.statusCode != 200) {
                grunt.fail.fatal('Could not get salt from ' + wpApi + endpoint);
            }

            // Write salt
            wpConfig = wpConfig.replace(tag, body);
            grunt.config('templates.wpConfig', wpConfig);
            grunt.log.ok('Salt added successfully!');

            done();
        });
    });

    /**
     * A task to set the locale in the config-template
     */
    grunt.registerTask('configureLocale', function () {
        'use strict';

        // Dependencies
        this.requiresConfig(
            'templates.wpConfig',
            'project.locale',
            'templateTags.locale'
        );

        // Get the config
        var wpConfig = grunt.config('templates.wpConfig');
        var locale = grunt.config('project.locale');
        var tag = grunt.config('templateTags.locale');

        // Set the locale
        grunt.log.subhead('Setting language to ' + locale);
        wpConfig = wpConfig.replace(tag, locale);
        grunt.config('templates.wpConfig', wpConfig);
        grunt.log.ok('Language added successfully!');
    });

    /**
     * A task to write the config to wp-config.php
     */
    grunt.registerTask('writeConfig', function () {
        'use strict';

        // Dependencies
        this.requires([
            'configureEnvironments',
            'configureTablePrefix',
            'configureSalt',
            'configureLocale'
        ]);
        this.requiresConfig(
            'dirs.wordpress',
            'templates.wpConfig'
        );

        // Get the config
        var wpConfig = grunt.config('templates.wpConfig');
        var wordpressDir = grunt.config('dirs.wordpress');

        // Write the file
        grunt.log.subhead('Writing Wordpress config');
        grunt.file.write(wordpressDir + 'wp-config.php', wpConfig);
        grunt.log.ok('Wordpress config writte correctly to: ' + wordpressDir + 'wp-config.php');
    });
};