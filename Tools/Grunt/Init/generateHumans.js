/**
 * Tasks for generating a humans.txt file
 *
 * @author David Faber
 * @company Peritus Webdesign
 */

module.exports = function (grunt) {
    'use strict';

    // Load external tasks
    grunt.loadNpmTasks('grunt-contrib-copy');

    /**
     * Our combined task
     */
    grunt.registerTask('generateHumans', function () {
        'use strict';

        // Dependencies
        this.requiresConfig('templates.humans', 'dirs.wordpress');

        // Get the config
        var humansTemplate = grunt.config('templates.humans');
        var wordpress = grunt.config('dirs.wordpress');

        // Setup copy-config
        grunt.log.subhead('Configuring copy-task.');
        grunt.config(
            'copy.humans',
            {
                nonull: true,
                src: humansTemplate,
                dest: wordpress + 'robots.txt'
            }
        );

        grunt.log.ok('All configured!');

        // Trigger the copy-task
        grunt.task.run('copy:humans');
    });
};