/**
 * Tasks to create a the project.json file
 *
 * @author David Faber
 * @company Peritus Webdesign
 */
module.exports = function (grunt) {
    'use strict';

    // Load external tasks
    grunt.loadNpmTasks('grunt-prompt');

    /**
     * Our combined task
     */
    grunt.registerTask('projectJSON', ['setupProjectPrompt', 'prompt:project']);

    /**
     * Task to configure the 'form'
     */
    grunt.registerTask('setupProjectPrompt', function () {
        'use strict';

        var defaults = grunt.file.readJSON('Settings/defaultProjectJSON.json');

        grunt.config(
            'prompt.project',
            {
                options: {
                    questions: [
                        {
                            config: 'projectPrompt.name',
                            type: 'input',
                            message: 'Project Name:',
                            validate: function (value) {
                                'use strict';

                                if (value.length) {
                                    return true;
                                } else {
                                    return 'Project name MUST NOT be empty!';
                                }
                            }
                        },
                        {
                            config: 'projectPrompt.slug',
                            type: 'input',
                            message: 'Project Slug:',
                            validate: function (value) {
                                'use strict';

                                if (value.length) {
                                    return true;
                                } else {
                                    return 'Project slug MUST NOT be empty!';
                                }
                            }
                        },
                        {
                            config: 'projectPrompt.url',
                            type: 'input',
                            message: 'Project Domain-name (NO http://, TLD, subdomains or www, e.g. foo for http://www.foo.bar):',
                            validate: function (value) {
                                'use strict';

                                if (value.length) {
                                    return true;
                                } else {
                                    return 'Domain MUST NOT be empty!';
                                }
                            }
                        },
                        {
                            config: 'projectPrompt.subdomain',
                            type: 'input',
                            message: 'Project Subdomain (can be empty):'
                        },
                        {
                            config: 'projectPrompt.tld',
                            type: 'input',
                            message: 'Project TLD (e.g. bar for foo.bar):',
                            validate: function (value) {
                                'use strict';

                                if (value.length) {
                                    return true;
                                } else {
                                    return 'TLD MUST NOT be empty!';
                                }
                            }
                        },
                        {
                            config: 'projectPrompt.www',
                            type: 'confirm',
                            message: 'Is the main domain www-prefixed:',
                            default: defaults.www
                        },
                        {
                            config: 'projectPrompt.locale',
                            type: 'input',
                            message: 'Project Locale (e.g. en_US):',
                            default: defaults.locale
                        },
                        {
                            config: 'projectPrompt.repository',
                            type: 'input',
                            message: 'Project GIT-Repository:',
                            validate: function (value) {
                                'use strict';

                                if (value.length) {
                                    return true;
                                } else {
                                    return 'GIT-Repository MUST NOT be empty!';
                                }
                            }
                        },
                        // Environments
                        // DEV
                        {
                            config: 'projectPrompt.environments.dev.env',
                            type: 'input',
                            message: 'Development APPLICATION_ENV:',
                            default: defaults.environments.dev.env
                        },
                        {
                            config: 'projectPrompt.environments.dev.domainCheck',
                            type: 'confirm',
                            message: 'Development DOMAIN_CHECK?',
                            default: defaults.environments.dev.domainCheck
                        },
                        {
                            config: 'projectPrompt.environments.dev.domain',
                            type: 'input',
                            message: 'Development domain:',
                            default: defaults.environments.dev.domain
                        },
                        {
                            config: 'projectPrompt.environments.dev.debug',
                            type: 'confirm',
                            message: 'Debug-Mode be turned on?',
                            default: defaults.environments.dev.debug
                        },
                        {
                            config: 'projectPrompt.environments.dev.mysql.db',
                            type: 'input',
                            message: 'Development MySQL db-name:',
                            default: function (answers) {
                                'use strict';

                                return answers['projectPrompt.slug'];
                            }
                        },
                        {
                            config: 'projectPrompt.environments.dev.mysql.user',
                            type: 'input',
                            message: 'Development MySQL user:',
                            default: defaults.environments.dev.mysql.user
                        },
                        {
                            config: 'projectPrompt.environments.dev.mysql.password',
                            type: 'input',
                            message: 'Development MySQL password:',
                            default: defaults.environments.dev.mysql.password
                        },
                        {
                            config: 'projectPrompt.environments.dev.mysql.host',
                            type: 'input',
                            message: 'Development MySQL host:',
                            default: defaults.environments.dev.mysql.host
                        },
                        {
                            config: 'projectPrompt.answers.devSSH',
                            type: 'confirm',
                            message: 'Do you want to configure SSH for this environment?',
                            default: defaults.environments.dev.devSSH
                        },
                        {
                            config: 'projectPrompt.environments.dev.ssh.host',
                            type: 'input',
                            message: 'Development SSH host:',
                            default: defaults.environments.dev.ssh.host,
                            when: function (answers) {
                                'use strict';

                                return answers['projectPrompt.answers.devSSH'];
                            }
                        },
                        {
                            config: 'projectPrompt.environments.dev.ssh.user',
                            type: 'input',
                            message: 'Development SSH user:',
                            default: defaults.environments.dev.ssh.user,
                            when: function (answers) {
                                'use strict';

                                return answers['projectPrompt.answers.devSSH'];
                            }
                        },
                        {
                            config: 'projectPrompt.environments.dev.ssh.key',
                            type: 'input',
                            message: 'Development SSH keyfile:',
                            default: defaults.environments.dev.ssh.key,
                            when: function (answers) {
                                'use strict';

                                return answers['projectPrompt.answers.devSSH'];
                            }
                        },
                        {
                            config: 'projectPrompt.environments.dev.paths.config',
                            type: 'input',
                            message: 'Development Path to apache-configs:',
                            default: defaults.environments.dev.paths.config
                        },
                        {
                            config: 'projectPrompt.environments.dev.paths.project',
                            type: 'input',
                            message: 'Development Path to projects:',
                            default: defaults.environments.dev.paths.project
                        },

                        // INTEGRATION
                        {
                            config: 'projectPrompt.environments.integration.env',
                            type: 'input',
                            message: 'Integration APPLICATION_ENV:',
                            default: defaults.environments.integration.env
                        },
                        {
                            config: 'projectPrompt.environments.integration.domainCheck',
                            type: 'confirm',
                            message: 'Integration DOMAIN_CHECK?',
                            default: defaults.environments.integration.domainCheck
                        },
                        {
                            config: 'projectPrompt.environments.integration.domain',
                            type: 'input',
                            message: 'Integration domain:',
                            default: defaults.environments.integration.domain
                        },
                        {
                            config: 'projectPrompt.environments.integration.debug',
                            type: 'confirm',
                            message: 'Debug-Mode be turned on?',
                            default: defaults.environments.integration.debug
                        },
                        {
                            config: 'projectPrompt.environments.integration.mysql.db',
                            type: 'input',
                            message: 'Integration MySQL db-name:',
                            default: function (answers) {
                                return answers['projectPrompt.slug'];
                            }
                        },
                        {
                            config: 'projectPrompt.environments.integration.mysql.user',
                            type: 'input',
                            message: 'Integration MySQL user:',
                            default: defaults.environments.integration.mysql.user
                        },
                        {
                            config: 'projectPrompt.environments.integration.mysql.password',
                            type: 'input',
                            message: 'Integration MySQL password:',
                            default: defaults.environments.integration.mysql.password
                        },
                        {
                            config: 'projectPrompt.environments.integration.mysql.host',
                            type: 'input',
                            message: 'Integration MySQL host:',
                            default: defaults.environments.integration.mysql.host
                        },
                        {
                            config: 'projectPrompt.answers.integrationSSH',
                            type: 'confirm',
                            message: 'Do you want to configure SSH for this environment?',
                            default: defaults.environments.integration.integrationSSH
                        },
                        {
                            config: 'projectPrompt.environments.integration.ssh.host',
                            type: 'input',
                            message: 'Integration SSH host:',
                            default: defaults.environments.integration.ssh.host,
                            when: function (answers) {
                                'use strict';

                                return answers['projectPrompt.answers.integrationSSH'];
                            }
                        },
                        {
                            config: 'projectPrompt.environments.integration.ssh.user',
                            type: 'input',
                            message: 'Integration SSH user:',
                            default: defaults.environments.integration.ssh.user,
                            when: function (answers) {
                                'use strict';

                                return answers['projectPrompt.answers.integrationSSH'];
                            }
                        },
                        {
                            config: 'projectPrompt.environments.integration.ssh.key',
                            type: 'input',
                            message: 'Integration SSH keyfile:',
                            default: defaults.environments.integration.ssh.key,
                            when: function (answers) {
                                'use strict';

                                return answers['projectPrompt.answers.integrationSSH'];
                            }
                        },
                        {
                            config: 'projectPrompt.environments.integration.paths.config',
                            type: 'input',
                            message: 'Integration Path to apache-configs:',
                            default: defaults.environments.integration.paths.config
                        },
                        {
                            config: 'projectPrompt.environments.integration.paths.project',
                            type: 'input',
                            message: 'Integration Path to projects:',
                            default: defaults.environments.integration.paths.project
                        },

                        // LIVE
                        {
                            config: 'projectPrompt.environments.live.env',
                            type: 'input',
                            message: 'Live APPLICATION_ENV:',
                            default: defaults.environments.live.env
                        },
                        {
                            config: 'projectPrompt.environments.live.debug',
                            type: 'confirm',
                            message: 'Debug-Mode be turned on?',
                            default: defaults.environments.live.debug
                        },
                        {
                            config: 'projectPrompt.environments.live.mysql.db',
                            type: 'input',
                            message: 'Live MySQL db-name:',
                            default: function (answers) {
                                'use strict';

                                return answers['projectPrompt.slug'];
                            }
                        },
                        {
                            config: 'projectPrompt.environments.live.mysql.user',
                            type: 'input',
                            message: 'Live MySQL user:',
                            default: defaults.environments.live.mysql.user
                        },
                        {
                            config: 'projectPrompt.environments.live.mysql.password',
                            type: 'input',
                            message: 'Live MySQL password:',
                            default: defaults.environments.live.mysql.password
                        },
                        {
                            config: 'projectPrompt.environments.live.mysql.host',
                            type: 'input',
                            message: 'Live MySQL host:',
                            default: defaults.environments.live.mysql.host
                        },
                        {
                            config: 'projectPrompt.answers.liveSSH',
                            type: 'confirm',
                            message: 'Do you want to configure SSH for this environment?',
                            default: defaults.environments.live.liveSSH
                        },
                        {
                            config: 'projectPrompt.environments.live.ssh.host',
                            type: 'input',
                            message: 'Live SSH host:',
                            default: defaults.environments.live.ssh.host,
                            when: function (answers) {
                                'use strict';

                                return answers['projectPrompt.answers.liveSSH'];
                            }
                        },
                        {
                            config: 'projectPrompt.environments.live.ssh.user',
                            type: 'input',
                            message: 'Live SSH user:',
                            default: defaults.environments.live.ssh.user,
                            when: function (answers) {
                                'use strict';

                                return answers['projectPrompt.answers.liveSSH'];
                            }
                        },
                        {
                            config: 'projectPrompt.environments.live.ssh.key',
                            type: 'input',
                            message: 'Live SSH keyfile:',
                            default: defaults.environments.live.ssh.key,
                            when: function (answers) {
                                'use strict';

                                return answers['projectPrompt.answers.liveSSH'];
                            }
                        },
                        {
                            config: 'projectPrompt.environments.live.paths.config',
                            type: 'input',
                            message: 'Live Path to apache-configs:',
                            default: defaults.environments.live.paths.config
                        },
                        {
                            config: 'projectPrompt.environments.live.paths.project',
                            type: 'input',
                            message: 'Live Path to projects:',
                            default: defaults.environments.live.paths.project
                        }
                    ],
                    then: function (results, done) {
                        'use strict';

                        var projectObj = grunt.config('projectPrompt');
                        delete projectObj.answers;
                        var projectJSON = JSON.stringify(projectObj, null, 4);

                        grunt.log.subhead('Writing data to project.json');
                        grunt.file.write('project.json', projectJSON);
                        grunt.log.ok('You now have a project.json');

                        done();
                    }
                }
            }
        );
    })
};