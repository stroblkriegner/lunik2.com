/**
 * Tasks for generating an .htaccess file
 *
 * @author David Faber
 * @company Peritus Webdesign
 */

module.exports = function (grunt) {
    'use strict';

    // Load external tasks
    grunt.loadNpmTasks('grunt-contrib-copy');

    /**
     * Our combined task
     */
    grunt.registerTask('generateHtaccess', function () {
        'use strict';

        // Dependencies
        this.requiresConfig('templates.htaccessBase', 'dirs.wordpress');

        // Get the config
        var htaccessTemplate = grunt.config('templates.htaccessBase');
        var wordpress = grunt.config('dirs.wordpress');

        // Setup copy-config
        grunt.log.subhead('Configuring copy-task.');
        grunt.config(
            'copy.htaccessBase',
            {
                nonull: true,
                src: htaccessTemplate,
                dest: wordpress + '.htaccess'
            }
        );

        grunt.log.ok('All configured!');

        // Trigger the copy-task
        grunt.task.run('copy:htaccessBase');
    });
};