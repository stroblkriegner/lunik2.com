/**
 * Tasks for generating a robots.txt file
 *
 * @author David Faber
 * @company Peritus Webdesign
 */

module.exports = function (grunt) {
    'use strict';

    // Load external tasks
    grunt.loadNpmTasks('grunt-contrib-copy');

    /**
     * Our combined task
     */
    grunt.registerTask('generateRobots', function () {
        'use strict';

        // Dependencies
        this.requiresConfig('templates.robots', 'dirs.wordpress');

        // Get the config
        var robotsTemplate = grunt.config('templates.robots');
        var wordpress = grunt.config('dirs.wordpress');

        // Setup copy-config
        grunt.log.subhead('Configuring copy-task.');
        grunt.config(
            'copy.robots',
            {
                nonull: true,
                src: robotsTemplate,
                dest: wordpress + 'robots.txt'
            }
        );

        grunt.log.ok('All configured!');

        // Trigger the copy-task
        grunt.task.run('copy:robots');
    });
};