/**
 * Tasks to remove unnecessary stuff from a fresh Wordpress-Installation
 *
 * @author David Faber
 * @company Peritus Webdesign
 */

module.exports = function (grunt) {
    'use strict';

    /**
     * Our combined task
     */
    grunt.registerTask('cleanupWP', ['removeDefaultThemes', 'removeDefaultPlugins']);

    /**
     * A task to remove the default Wordpress themes from a fresh install
     */
    grunt.registerTask('removeDefaultThemes', function () {
        'use strict';

        // Dependencies
        this.requiresConfig('dirs.themes');

        // Get the config
        var themesDir = grunt.config('dirs.themes');

        // Delete default themes
        grunt.log.subhead('Deleting default themes.');
        grunt
            .file
            .expand(
                {
                    filter: 'isDirectory',
                    cwd: themesDir
                },
                'twenty*'
            )
            .forEach(function (path) {
                grunt.file.delete(themesDir + path);
                grunt.log.ok('Successfully deleted theme: ' + path);
            });

        grunt.log.ok('All default themes deleted');
    });

    /**
     * A task to remove the default Wordpress plugins from a fresh install
     */
    grunt.registerTask('removeDefaultPlugins', function () {
        'use strict';

        // Dependencies
        this.requiresConfig('dirs.plugins');

        // Get the config
        var pluginsDir = grunt.config('dirs.plugins');

        // Delete default plugins
        grunt.log.subhead('Deleting default themes.');
        grunt
            .file
            .expand(
                {
                    cwd: pluginsDir
                },
                [
                    'hello.php',
                    'akismet'
                ]
            )
            .forEach(function (path) {
                grunt.file.delete(pluginsDir + path);
                grunt.log.ok('Successfully deleted plugin: ' + path);
            });

        grunt.log.ok('All default plugins deleted');
    });
};