/**
 * Tasks to get current Version of Wordpress and download it
 *
 * @author David Faber
 * @company Peritus Webdesign
 */
var request = require('request-json');

module.exports = function (grunt) {
    'use strict';

    // Load external tasks
    grunt.loadNpmTasks('grunt-zip');
    grunt.loadNpmTasks('grunt-contrib-rename');

    /**
     * Our combined task
     */
    grunt.registerTask(
        'getWP',
        [
            'getCurrentWP',
            'downloadCurrentWP',
            'unzipWP',
            'cleanupWPDownload'
        ]
    );

    /**
     * A task to get the current version of Wordpress from the Wordpress API
     */
    grunt.registerTask('getCurrentWP', function () {
        'use strict';

        // Dependencies
        this.requiresConfig('wpApi.url', 'wpApi.version', 'project.locale', 'wpRepo.url');

        // Get the config
        var wpApi = grunt.config('wpApi');
        var locale = grunt.config('project.locale');
        var wpRepo = grunt.config('wpRepo.url');

        // This task is async
        var done = this.async();

        // Get current version
        grunt.log.subhead('Getting current version of Worpdress.')
        var wpApiClient = request.newClient(wpApi.url);
        wpApiClient.get(wpApi.version + '?locale=' + locale, '', function (error, response, body) {
            'use strict';

            if (error) {
                grunt.fail.fatal('Could not get current wordpress version from: ' + wpApi.url + wpApi.version);
            }

            if (body.offers[0]) {
                var wp = body.offers[0];
                var downloadLink = wp.download.replace(wpRepo, '');

                grunt.config('wpRepo.download', downloadLink);

                grunt.log.ok('Current Version of Worpdress is: ' + wp.current);
                grunt.log.ok('Locale used is: ' + wp.locale);

                done();
            } else {
                grunt.fail.fatal('Cannot understand Worpdress-API-response... maybe it\'s clingon or so... ;)');
            }
        });
    });

    /**
     * A task to download the current version of Wordpress
     */
    grunt.registerTask('downloadCurrentWP', function () {
        'use strict';

        // Dependencies
        this.requiresConfig('wpRepo.url', 'wpRepo.download', 'dirs.wpZip');
        this.requires('getCurrentWP');

        // Get the config
        var wpRepo = grunt.config('wpRepo.url');
        var download = grunt.config('wpRepo.download');
        var wpZip = grunt.config('dirs.wpZip');

        // This task is async
        var done = this.async();

        // Download Wordpress
        grunt.log.subhead('Downloading Wordpress');
        var wpRepoClient = request.newClient(wpRepo);
        wpRepoClient.saveFile(wpRepo + download, wpZip, function (error, response, body) {
            if (error) {
                grunt.fail.fatal('Could not download Wordpress from: ' + wpRepo + download);
            }

            grunt.log.ok('Wordpress downloaded correctly.');
            done();
        });
    });

    /**
     * A task to unpack zip and rename the folder from wordpress to Public
     */
    grunt.registerTask('unzipWP', function () {
        'use strict';

        // Dependencies
        this.requiresConfig('dirs.wpZip', 'dirs.wordpress');

        // Get the config
        var dirs = grunt.config('dirs');

        // Check if wordpress.zip is there
        if (!grunt.file.exists(dirs.wpZip)) {
            grunt.fail.fatal('There is no file ' + dirs.wpZip + '. Run \'grunt getWP\' instead.');
        }

        // Configure unzip and rename tasks
        grunt.log.subhead('Configuring unzip & rename tasks.');
        grunt.config(
            'unzip.wordpress',
            {
                src: dirs.wpZip,
                dest: './'
            }
        );

        grunt.config(
            'rename.wordpress',
            {
                src: './wordpress',
                dest: './' + dirs.wordpress
            }
        );

        grunt.log.ok('Everything configured.');

        // Trigger unzip and rename task
        grunt.task.run('unzip:wordpress', 'rename:wordpress');
    });

    /**
     * Cleanup after install
     */
    grunt.registerTask('cleanupWPDownload', function () {
        'use strict';

        // Dependencies
        this.requiresConfig('dirs.wpZip');

        // Get the config
        var wpZip = grunt.config('dirs.wpZip');

        grunt.log.subhead('Wait a second, I need to clean up first ;)');
        grunt.file.delete(wpZip);
        grunt.log.ok('All done!');
    });
};