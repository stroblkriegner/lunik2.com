/**
 * Building Sass-Stuff
 *
 * @author David Faber
 * @company Peritus Webdesign
 */

module.exports = function (grunt) {
    'use strict';

    // Extend the config for sass
    grunt.config(
        'sass',
        {
            dev: {
                files: {
                    'Public/wp-content/themes/basetheme-foundation/Public/css/style.css':
                        'Public/wp-content/themes/basetheme-foundation/Public/css/src/style.scss'
                }
            },
            live: {
                options: {
                    sourcemap: 'none',
                    style: 'compressed'
                },
                files: {
                    'Public/wp-content/themes/basetheme-foundation/Public/css/style.css':
                        'Public/wp-content/themes/basetheme-foundation/Public/css/src/style.scss'
                }
            }
        }
    );

    // Extend the config for sass-watch
    grunt.config(
        'watch.sass',
        {
            files: ['Public/wp-content/themes/basetheme-foundation/Public/css/**/*.scss'],
            tasks: ['sass:dev']
        }
    );

    // Load external tasks
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
}