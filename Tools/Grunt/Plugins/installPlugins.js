/**
 * Install standard themes
 *
 * @author David Faber
 * @company Peritus Webdesign
 */
var request = require('request-json');
var async = require('async');

module.exports = function (grunt) {
    'use strict';

    // Load external tasks
    grunt.loadNpmTasks('grunt-prompt');
    grunt.loadNpmTasks('grunt-zip');

    // Configure Plugins
    var defaultPlugins = grunt.file.readJSON('Settings/plugins.json');

    /**
     * Our combined task
     */
    grunt.registerTask(
        'installPlugins',
        [
            'setupPluginPrompt',
            'prompt:plugins',
            'getPluginVersions',
            'downloadPlugins',
            'cleanupPlugins'
        ]
    );

    /**
     * Task to setup the default plugin prompt
     */
    grunt.registerTask('setupPluginPrompt', function () {
        'use strict';

        grunt.config(
            'prompt.plugins',
            {
                options: {
                    questions: [
                        {
                            config: 'plugins',
                            type: 'checkbox',
                            message: 'Which Plugins should be installed?',
                            choices: defaultPlugins
                        },
                        {
                            config: 'visualComposer',
                            type: 'confirm',
                            message: 'Do you want us to copy VisualComposer?',
                            default: true
                        }
                    ]
                }
            }
        );
    });

    /**
     * Task to get current versions & download-links of selected Plugins
     */
    grunt.registerTask('getPluginVersions', function () {
        'use strict';

        // Dependencies
        this.requires('prompt:plugins');
        this.requiresConfig('wpRepo.plugins', 'wpApi.url', 'wpApi.plugin', 'plugins');

        // Get the config
        var pluginsRepo = grunt.config('wpRepo.plugins');
        var wpApi = grunt.config('wpApi');
        var plugins = grunt.config('plugins');

        // This task is async
        var done = this.async();

        // Fetch plugin versions & download-links
        grunt.log.subhead('Fetching current plugin versions.');

        var pluginData = [];
        var calls = [];
        plugins.forEach(function (plugin) {
            'use strict';

            calls.push(function(callback) {
                'use strict';

                var wpApiClient = request.newClient(wpApi.url);
                wpApiClient.get(wpApi.plugin + plugin + '.json', '', function (error, response, body) {
                    'use strict';

                    if (error) {
                        grunt.fail.fatal('Could not get current plugin version from: ' + wpApi.url + wpApi.plugin + plugin + '.json');
                    }

                    if (body.name && body.download_link && body.version) {
                        pluginData.push({
                            slug: plugin,
                            name: body.name,
                            download: body.download_link.replace(pluginsRepo, ''),
                            version: body.version
                        });

                        grunt.log.subhead('Fetched ' + body.name);
                        grunt.log.ok('Current Version: ' + body.version);
                        grunt.log.ok('Download-Link: ' + body.download_link);
                    } else {
                        grunt.fail.fatal('Cannot understand Worpdress-API-response... maybe it\'s clingon or so... ;)');
                    }

                    callback(null, plugin);
                });
            });
        });

        // Wait for all tasks
        async.parallel(calls, function(err, result) {
            'use strict';

            grunt.config('plugins', pluginData);
            done();
        });
    });

    /**
     * Task to download all plugin-files
     */
    grunt.registerTask('downloadPlugins', function () {
        'use strict';

        // Dependencies
        this.requires('getPluginVersions');
        this.requiresConfig('wpRepo.plugins', 'plugins', 'dirs.plugins', 'files.visualComposer', 'visualComposer');

        // Get the config
        var pluginsRepo = grunt.config('wpRepo.plugins');
        var plugins = grunt.config('plugins');
        var pluginsDir = grunt.config('dirs.plugins');
        var visualComposerFile = grunt.config('files.visualComposer');
        var installVisualComposer = grunt.config('visualComposer');

        // This task is async
        var done = this.async();

        // Download plugin files
        grunt.log.subhead('Downloading plugin-files');

        var calls = [];
        plugins.forEach(function (plugin) {
            'use strict';

            calls.push(function(callback) {
                'use strict';

                var wpRepoClient = request.newClient(pluginsRepo);
                wpRepoClient.saveFile(pluginsRepo + plugin.download, pluginsDir + plugin.slug + '.zip', function (error, response, body) {
                    if (error) {
                        grunt.fail.fatal('Could not download ' + plugin.name + ' from: ' + pluginsRepo + plugin.download);
                    }

                    grunt.log.subhead(plugin.name);
                    grunt.log.ok(plugin.name + ' downloaded correctly.');

                    // Configure unzip-task
                    grunt.log.writeln('Configuring unzip task for ' + plugin.name + '.');
                    grunt.config(
                        'unzip.' + plugin.slug,
                        {
                            src: pluginsDir + plugin.slug + '.zip',
                            dest: pluginsDir
                        }
                    );
                    grunt.task.run('unzip:' + plugin.slug);
                    grunt.log.ok('Configured!');

                    callback(null, plugin);
                });
            });
        });

        // Wait for all tasks
        async.parallel(calls, function(err, result) {
            'use strict';

            // Install Visual Composer
            if (installVisualComposer) {
                grunt.log.subhead('Visual Composer');
                grunt.log.writeln('Installing Visual Composer');
                grunt.config(
                    'unzip.vc',
                    {
                        src: visualComposerFile,
                        dest: pluginsDir
                    }
                );
                grunt.task.run('unzip:vc');
            }

            done();
        });
    });

    /**
     * Task to remove downloaded zip-files
     */
    grunt.registerTask('cleanupPlugins', function () {
        'use strict';

        // Dependencies
        this.requiresConfig('dirs.plugins');

        // Get the config
        var pluginsDir = grunt.config('dirs.plugins');

        // This task is async
        var done = this.async();

        // Delete zip files
        grunt.log.subhead('Deleting all downloaded zip-files.');
        var files = grunt.file.expand(pluginsDir + '*.zip');

        files.forEach(function (file) {
            grunt.log.writeln(file);
            grunt.file.delete(file);
            grunt.log.ok('Deleted.');
        });

        done();
    });
};