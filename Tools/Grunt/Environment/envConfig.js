/**
 * Tasks for setting up an environment
 *
 * @author David Faber
 * @company Peritus Webdesign
 */
module.exports = function (grunt) {
    'use strict';

    var filename = '';

    // Load external tasks
    grunt.loadNpmTasks('grunt-exec');
    grunt.loadNpmTasks('grunt-ssh');

    /**
     * Our combined task
     *
     * @param string env The environment to configure
     * @param string withDB Set to 'db' if you want db to be created otherwise use action as second parameter
     * @param action action (optional) The action to run after apache-config is built
     *      Possible values:
     *          noValue: Build, upload, test and reload apache-config
     *          'echo': Output apache-config to command line
     *          'write': Output the apache-config to a file
     *          'start': Like empty + start browser
     */
    grunt.registerTask('envConfig', function (env, withDB, action) {
        'use strict';

        // check if environment is given and exists
        if (!env) {
            grunt.fail.fatal('No environment given!');
        }

        if (withDB && withDB !== 'db' && !action) {
            action = withDB;
        }

        // Run the tasks
        grunt.task.run('buildApacheConfig:' + env);

        if (action !== 'echo' && action !== 'write') {
            grunt.task.run(
                'writeApacheConfig:' + env,
                'uploadApacheConfig:' + env,
                'reloadApacheConfig:' + env,
                'cleanupApacheConfig'
            );
        }

        if (withDB == 'db') {
            grunt.task.run('createDB:' + env);
        }

        if (action == 'start') {
            grunt.task.run('openEnvironment:' + env);
        } else if (action == 'echo') {
            grunt.task.run('echoApacheConfig:' + env);
        } else if (action == 'write') {
            grunt.task.run('writeApacheConfig:' + env);
        }
    });

    /**
     * Task to generate and upload the server-config
     *
     * @param string env The environment to configure
     */
    grunt.registerTask('buildApacheConfig', function (env) {
        'use strict';

        // Dependencies
        if (!env) {
            grunt.fail.fatal('No environment given!');
        }
        this.requiresConfig(
            'templates.apacheConfig',
            'project.slug',
            'project.tld',
            'project.url',
            'project.environments.' + env + '.env',
            'project.environments.' + env + '.domain',
            'project.environments.' + env + '.paths.project',
            'templateTags.projectFolder',
            'templateTags.slug',
            'templateTags.environment',
            'templateTags.domain',
            'dirs.wordpress'
        );

        // Get the config
        var apacheConf = grunt.config('templates.apacheConfig');
        var project = grunt.config('project');
        var environment = grunt.config('project.environments.' + env + '.env');
        var domain = grunt.config('project.environments.' + env + '.domain');
        var projectPath = grunt.config('project.environments.' + env + '.paths.project');
        var templateTags = grunt.config('templateTags');
        var wpDir = grunt.config('dirs.wordpress');

        // Generate the project folder
        grunt.log.subhead('Building Apache-config');
        var projectFoler = projectPath;
        if (project.subdomain) {
            projectFoler += project.subdomain + '.'
        }
        projectFoler += project.url + '.' + project.tld + '/' + wpDir;

        // Update config
        apacheConf = replaceAll(templateTags.projectFolder, projectFoler, apacheConf);
        apacheConf = replaceAll(templateTags.slug, project.slug, apacheConf);
        apacheConf = replaceAll(templateTags.domain, domain, apacheConf);
        apacheConf = replaceAll(templateTags.environment, environment, apacheConf);

        // Write config
        grunt.config('templates.apacheConfig', apacheConf);

        grunt.log.ok('Apache config built successfully.');
    });

    /**
     * A task create an Apache-config-file
     *
     * @param string env The environment to configure
     */
    grunt.registerTask('writeApacheConfig', function (env) {
        'use strict';

        // Dependencies
        this.requires('buildApacheConfig:' + env);
        this.requiresConfig(
            'templates.apacheConfig',
            'project.tld',
            'project.url'
        );
        
        // Get the config
        var apacheConfig = grunt.config('templates.apacheConfig');
        var subdomain = grunt.config('project.subdomain');
        var tld = grunt.config('project.tld');
        var url = grunt.config('project.url');

        // Write file
        setFilename();
        grunt.log.subhead('Writing Apache-config to file.');
        grunt.file.write(filename, apacheConfig);
        grunt.log.ok('Successfully created Apache-config-file: ' + filename);
    });

    /**
     * A task to echo the Apache-config-file
     *
     * @param string env The environment to configure
     */
    grunt.registerTask('echoApacheConfig', function (env) {
        'use strict';

        // Dependencies
        this.requires('buildApacheConfig:' + env);
        this.requiresConfig('templates.apacheConfig');

        // Get the config
        var apacheConfig = grunt.config('templates.apacheConfig');

        // Echo the file
        grunt.log.subhead('Your Apache-config:\n\n\n');
        grunt.log.write(apacheConfig);
        grunt.log.write('\n\n\n');
        grunt.log.ok('Copy code to create a server-config');
    });

    /**
     * A task to upload the server-config to
     *
     * @param string env The environment to configure
     */
    grunt.registerTask('uploadApacheConfig', function (env) {
        'use strict';

        // Dependencies
        this.requiresConfig(
            'project.environments.' + env + '.paths.config',
            'project.environments.' + env + '.ssh.host',
            'project.environments.' + env + '.ssh.user',
            'project.environments.' + env + '.ssh.key',
            'project.tld',
            'project.url'
        );

        // Check if file exists
        setFilename();
        if (!grunt.file.exists(filename)) {
            grunt.fail.fatal('There is no file ' + filename);
        }

        // Get the config
        var remoteDir = grunt.config('project.environments.' + env + '.paths.config');
        var ssh = grunt.config('project.environments.' + env + '.ssh');

        // Upload the file
        var sftpConfig = {
            files: {
                '/': filename
            },
            options: {
                path: remoteDir,
                host: ssh.host,
                username: ssh.user,
                privateKey: grunt.file.read(ssh.key)
            }
        };
        grunt.config('sftp.config', sftpConfig);

        grunt.task.run('sftp:config');
    });

    /**
     * Test & reload Apache-config
     *
     * @param string env The environment to reload
     */
    grunt.registerTask('reloadApacheConfig', function (env) {
        'use strict';

        // Dependencies
        this.requiresConfig(
            'project.environments.' + env + '.ssh.host',
            'project.environments.' + env + '.ssh.user',
            'project.environments.' + env + '.ssh.key',
            'commands.apache.testConfig',
            'commands.apache.reloadConfig'
        );

        // Get the config
        var ssh = grunt.config('project.environments.' + env + '.ssh');
        var commands = grunt.config('commands.apache');

        // Configure tasks
        grunt.log.subhead('Configuring sshexec tasks.');
        grunt.config(
            'sshexec',
            {
                testApacheConfig: {
                    command: commands.testConfig,
                    options: {
                        host: ssh.host,
                        username: ssh.user,
                        privateKey: grunt.file.read(ssh.key)
                    }
                },
                reloadApacheConfig: {
                    command: commands.reloadConfig,
                    options: {
                        host: ssh.host,
                        username: ssh.user,
                        privateKey: grunt.file.read(ssh.key)
                    }
                }
            }
        );

        // Trigger the sshexec tasks
        grunt.task.run('sshexec:testApacheConfig', 'sshexec:reloadApacheConfig');
    });

    /**
     * Test & restart Apache-config
     *
     * @param string env The environment to restart
     */
    grunt.registerTask('restartApacheConfig', function (env) {
        'use strict';

        // Dependencies
        this.requiresConfig(
            'project.environments.' + env + '.ssh.host',
            'project.environments.' + env + '.ssh.user',
            'project.environments.' + env + '.ssh.key',
            'commands.apache.testConfig',
            'commands.apache.restart'
        );

        // Get the config
        var ssh = grunt.config('project.environments.' + env + '.ssh');
        var commands = grunt.config('commands.apache');

        // Configure sshexec tasks
        grunt.log.subhead('Configuring sshexec tasks.');
        grunt.config(
            'sshexec',
            {
                testApacheConfig: {
                    command: commands.testConfig,
                    options: {
                        host: ssh.host,
                        username: ssh.user,
                        privateKey: grunt.file.read(ssh.key)
                    }
                },
                restartApache: {
                    command: commands.reloadConfig,
                    options: {
                        host: ssh.host,
                        username: ssh.user,
                        privateKey: grunt.file.read(ssh.key)
                    }
                }
            }
        );

        // Trigger the sshexec tasks
        grunt.task.run('sshexec:testApacheConfig', 'sshexec:restartApache');
    });


    /**
     * Open the environment in the standard browser
     *
     * @param string env The environment to reload
     */
    grunt.registerTask('openEnvironment', function (env) {
        'use strict';

        if (env !== 'live') {
            // Dependencies
            this.requiresConfig(
                'project.environments.' + env + '.domain',
                'project.slug'
            );

            // Get the config
            var slug = grunt.config('project.slug');
            var domain = grunt.config('project.environments.' + env + '.domain');

            // Build the url
            var url = 'http://' + slug + domain;
        } else {
            // Dependencies
            this.requiresConfig(
                'project.url',
                'project.tld'
            );

            // Get the config
            var project = grunt.config('project');

            // Build the url
            var url = 'http://';

            if (project.www) {
                url += 'www.';
            }

            if (project.subdomain) {
                url += project.subdomain + '.';
            }

            url += project.url + project.tld;
        }

        grunt.log.subhead('Opening ' + url + ' in your standard browser!');
        // Build exec-config to replace remote 'origin'
        grunt.log.subhead('Configuring tasks to open website in your stanard browser!');
        var execConfig = {
            openBrowser: 'open ' + url
        };

        // Save the config
        grunt.config('exec', execConfig);
        grunt.log.ok('All configured!');

        // Run the task
        grunt.task.run('exec:openBrowser');
    });

    /**
     * Cleanup files
     */
    grunt.registerTask('cleanupApacheConfig', function () {
        'use strict';

        // Dependencies
        this.requiresConfig(
            'templates.apacheConfig',
            'project.tld',
            'project.url'
        );

        grunt.log.subhead('Cleaning up apache-config.');

        // Delete the file
        setFilename();
        grunt.file.delete(filename);
    });

    /**
     * Build the apache-config filename from tld, url & subdomain
     */
    function setFilename() {
        if (filename) {
            return;
        }

        // Get the config
        var subdomain = grunt.config('project.subdomain');
        var tld = grunt.config('project.tld');
        var url = grunt.config('project.url');

        // Build the filename
        if (subdomain) {
            filename += subdomain + '_';
        }

        filename += url + '_' + tld;
    }

    /**
     * Helper function to replace all
     *
     * @param find
     * @param replace
     * @param str
     *
     * @returns {*|XML|string|void}
     */
    function replaceAll(find, replace, str) {
        'use strict';

        var find = find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
        return str.replace(new RegExp(find, 'g'), replace);
    }
};