/**
 * Tasks to setup git for the project
 *
 * @author David Faber
 * @company Peritus Webdesign
 */
module.exports = function (grunt) {
    'use strict';

    // Load external tasks
    grunt.loadNpmTasks('grunt-exec');

    /**
     * Our combined task
     */
    grunt.registerTask('initGit', ['resetGit', 'gitIgnore', 'setupRepo', 'pushAll']);

    /**
     * Task to reset git
     */
    grunt.registerTask('resetGit', function () {
        'use strict';

        // Build exec-config to replace reset git
        grunt.log.subhead('Configuring tasks to remove .git and re-init.');
        var execConfig =  {
            resetGit: 'rm -rf .git',
            initGit: 'git init'
        };

        // Save the config
        grunt.config('exec', execConfig);
        grunt.log.ok('All configured!');

        // Execute the tasks
        grunt.task.run(['exec:resetGit', 'exec:initGit']);
    });

    /**
     * Task to replace the base-project .gitignore with the correct one
     */
    grunt.registerTask('gitIgnore', function () {
        'use strict';

        // Dependencies
        this.requiresConfig('templates.gitIgnore');

        // Get the config
        var gitignoreTemplate = grunt.config('templates.gitIgnore');

        // Setup copy-config
        grunt.log.subhead('Configuring copy-task for the correct .gitignore.');
        grunt.config(
            'copy.gitignore',
            {
                nonull: true,
                src: gitignoreTemplate,
                dest: '.gitignore'
            }
        );

        grunt.log.ok('All configured!');

        // Trigger the copy-task
        grunt.task.run('copy:gitignore');
    });

    /**
     * Task for removing current origin and replacing it with project origin
     */
    grunt.registerTask('setupRepo', function () {
        'use strict';

        // Dependencies
        this.requiresConfig('project.repository');

        // Get the config
        var repo = grunt.config('project.repository');

        // Build exec-config to replace remote 'origin'
        grunt.log.subhead('Configuring tasks to set remote \'origin\' to: ' + repo);
        var execConfig = {
            newOrigin: 'git remote add origin ' + repo
        };

        // Save the config
        grunt.config('exec', execConfig);
        grunt.log.ok('All configured!');

        // Execute the tasks
        grunt.task.run(['exec:newOrigin']);
    });

    /**
     * Our Task for making and inital commit including the push
     */
    grunt.registerTask('pushAll', function () {
        'use strict';

        // Build exec-config to make an initial commit and push
        grunt.log.subhead('Configuring tasks to make an initial commit and push.');
        var execConfig = {
            gitAddAll: 'git add . --all',
            gitCommit: 'git commit -q -m "Initial Commit"',
            gitPush: 'git push --force origin master'
        };

        // Save the config
        grunt.config('exec', execConfig);
        grunt.log.ok('All configured!');

        // Execute the tasks
        grunt.task.run(['exec:gitAddAll', 'exec:gitCommit', 'exec:gitPush']);
    });
};